// In App.js in a new project

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';

import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Home } from './screens/Home';
import { PhotoItem } from './screens/PhotoItem';
import { Provider } from 'react-redux';
import { store } from './store/index'

export default App = () => {

  const Stack = createStackNavigator();

  return (
    <>
      <Provider store={store}>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Photo item" component={PhotoItem} />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    </>
  );
};