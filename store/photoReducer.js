const defaultState = {
    photo: 'photo'
};

export const photoReducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'SET_PHOTO':
            return { ...state, photo: action.payload}

        default:
            return state;
    }
}