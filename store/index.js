const { createReducer } = require("@reduxjs/toolkit");
const { createStore, combineReducers } = require("redux");
const { photoReducer } = require("./photoReducer");

export const store = createStore(photoReducer);