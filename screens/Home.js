import 'react-native-gesture-handler';

import React, { useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, FlatList } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';



function Home({ navigation }) {
  const [data, setData] = useState([]);

  const dispatch = useDispatch();
  const photo = useSelector(state => state.photo)

  const photoItem = (imgSource) => {
    navigation.navigate('Photo item'),
      dispatch({
        type: 'SET_PHOTO',
        payload: imgSource,
      })
  }

  useEffect(() => {
    fetch('https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0')
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
  }, []);

  return (
    <>
      <View style={styles.container}>
        <FlatList
          numColumns={2}
          data={data}
          keyExtractor={({ id }, index) => id}
          renderItem={({ item }) => (
            <TouchableWithoutFeedback onPress={() => photoItem(item.urls.regular)}>
              <View key={item.urls.regular} style={styles.item}>
                <Image style={styles.img} source={{ uri: item.urls.regular }} />
                <Text>By {item.user.username}</Text>
              </View>
            </TouchableWithoutFeedback>
          )}
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  img: {
    width: 130,
    height: 170,
    margin: 10,
  },
  item: {
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingBottom: 30,
    alignItems: 'center',
  },
  container: {
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
  }
})

export { Home }