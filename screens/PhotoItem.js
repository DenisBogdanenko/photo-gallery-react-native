import 'react-native-gesture-handler';

import * as React from 'react';
import { View, Text, Button, StyleSheet, Image, Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

function PhotoItem({ navigation }) {

  const dispatch = useDispatch();
  const photo = useSelector(state => state.photo)
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

      <Image style={styles.img} source={{ uri: photo }} />
    </View>
  );
}

const styles = StyleSheet.create({
  img: {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
  },
})

export { PhotoItem }